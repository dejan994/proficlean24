<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">

		<div class="carousel-inner" role="listbox">
			<!--- Slide 1 -->
			<div class="carousel-item contact-carousel active">
				<picture>
					<source srcset="img/unternehmen-1.jpg" type="image/webp">
					<source srcset="img/unternehmen-1.jpg" type="image/jpeg">
					<img class="d-block w-100" src="img/unternehmen-1.jpg">
				</picture>
				<div class="carousel-caption text-center">
					<h1 class="animate__animated animate__fadeInDown animate__delay-1s">ProfiClean24</h1>
					<h3 class="animate__animated animate__fadeInUp animate__delay-2s">Ihre Saubere Lösung</h3>
				</div>
			</div>
		</div> <!--- End carousel inner -->
	</div>
</div>

<div id="unternehmen" class="text-under-carousel">

	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h1 >Unternehmen:</h1>
		</div>
		<div class="col-md-4"></div>
	</div>

	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Über uns</h2>
			<hr class="heading-underline-left">
			<h3>Wir, die Firma PROFICLEAN24 sind ein dynamisches Reinigungsunternehmen mit klarem Konzept und klarer Firmen Philosophie. Für uns steht Ihre Zufriedenheit im Vordergrund.</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Qualität und Flexibilität</h2>
			<hr class="heading-underline-left">
            <h3>Mit einem schlagkräftigen Team sind wir für Sie im Einsatz. Unsere ausgebildeten und regelmäßig geschulten Mitarbeiter erledigen alle anfallenden Reinigungsarbeiten schnell, gründlich, zuverlässig und absolut professionell.</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Verantwortung für Mensch und Umwelt</h2>
			<hr class="heading-underline-left">
			<h3>Motivierte und verlässiche Mitarbeiter sind das Resultat. Zum Schutze der Gesundheit und Umwelt achten wir auf den Einsatz von umweltverträglichen, ökologischen Reinigungsmitteln und -Methoden.</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Faire Preise</h2>
			<hr class="heading-underline-left">
			<h3>Unser hoher Qualitätsanspruch und Ihre Zufriedenheit stehen bei uns an oberster Stelle. Wir bieten Ihnen mehr als nur Sauberkeit - und das zu fairen preise! Überzeugen sie sich selbst und rufen uns an. <br><br> <b>PROFICLEAN24</b> - Ihr kompetenter Partner fur Gebäudereinigung!</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>



<!--- Start contact Section -->
<div id="contact">
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
</div>
<!--- End contact Section -->


<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>