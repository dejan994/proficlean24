<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">
		<div class="carousel-inner" role="listbox">
			<!--- Slide 1 -->
			<div class="carousel-item impressum-carousel active">
				<picture>
					<source srcset="img/impressum-1.webp" type="image/webp">
					<source srcset="img/impressum-1.jpg" type="image/jpeg">
					<img class="d-block w-100" src="img/impressum-1.jpg">
				</picture>
				<div class="carousel-caption text-center">
					<h1 class="animate__animated animate__fadeInDown animate__delay-1s">ProfiClean24</h1>
					<h3 class="animate__animated animate__fadeInUp animate__delay-2s">Ihre Saubere Lösung</h3>
				</div>
			</div>
		</div> <!--- End carousel inner -->
	</div>

</div>

<div id="impressum" class="text-under-carousel">

	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h1 >Impressum:</h1>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Angaben gemäß § 5 TMG:</h2>
			<hr class="heading-underline-left">
			<h3>Nikola Cujo<br>
                Plieninger Straße 55<br>
                70794 Filderstadt<br>
                <b>PROFICLEAN24</b></h3>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Kontakt:</h2>
			<hr class="heading-underline-left">
			<h3>Telefon: +4915734895786<br>
                E-mail: info@proficlean24.de</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Umsatszsteuer:</h2>
			<hr class="heading-underline-left">
			<h3>Steuernummer: 97018/30643<br>
				Finanzamt Stuttgart III</h3>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

<!--- Start contact Section -->
<div id="contact">
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
</div>
<!--- End contact Section -->


<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>
