<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>


</div>

<div id="inahber" class="text-under-carousel">

	<div class="row team-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h1 >Team:</h1>
			<hr class="heading-underline-left">
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<img class="portrait-photo" alt="Portrait" src="img/portrait.jpg" >
			<h3><b>Nikola Cujo</b> <br>
				<i class="italic-x">Geschäftsführer & Inhaber</i> <br>
				<br>
				Telefonnummer: <b>+4915734895786</b><br>
				<br>
				Email: <b>info@proficlean24.de</b> <br></h3>
		</div>
		<div class="col-md-4"></div>
	</div>

</div>

<!--- Start contact Section -->
<div id="contact">
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
</div>
<!--- End contact Section -->


<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>
