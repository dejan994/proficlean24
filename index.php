<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>

<!--- Start Image Slider-->

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		</ol>

		<div class="carousel-inner" role="listbox">
			<!--- Slide 1 -->
			<div class="carousel-item active">
				<picture>
					<source srcset="img/homepage/homepage-1.webp" type="image/webp">
					<source srcset="img/homepage/homepage-1.jpg" type="image/jpeg">
					<img class="d-block w-100" src="img/homepage/homepage-1.jpg">
				</picture>
				<div class="carousel-caption text-center">
					<h1 class="animate__animated animate__fadeInDown animate__delay-1s">ProfiClean24</h1>
					<h3 class="animate__animated animate__fadeInUp animate__delay-2s">Ihre Saubere Lösung</h3>
				</div>
			</div>
			<!--- Slide 2 -->
			<div class="carousel-item">
				<picture>
					<source srcset="img/homepage/homepage-2.webp" type="image/webp">
					<source srcset="img/homepage/homepage-2.jpg" type="image/jpeg">
					<img class="d-block w-100" src="img/homepage/homepage-2.jpg">
				</picture>
				<div class="carousel-caption text-center">
					<h1 class="animate__animated animate__fadeInDown animate__delay-1s">ProfiClean24</h1>
					<h3 class="animate__animated animate__fadeInUp animate__delay-2s">Ihre Saubere Lösung</h3>
				</div>
			</div>

		</div> <!--- End carousel inner -->
		<!-- Carousel Controls -->
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
		</a>
	</div>
</div>
<!--- End Home Section -->


<!-- Start Layout Section -->
<div id="layout">
	<div class="col-12 text-center">
		<h1>Arbeiten die Wir für Sie erledigen</h1>
		<h3>Professionell. <i class="fas fa-check-square"></i> Schnell. <i class="fas fa-check-square"></i> Zuverlässig. <i class="fas fa-check-square"></i></h3>
	</div>
</div>
<!-- End Layout Section -->

<div id="contact-homepage">
	<div class="col-12 text-center">
		<div class="row contact-icons">
			<div class="col-md">
				<i class="fas fa-map-marker-alt fa-3x" style="color:#c53500; margin-bottom: 1.5rem;"></i>
				<h5><a class="linkable" href="https://maps.google.com/?q=Plieninger Str.55,70794 Filderstadt">Plieninger Str.55, 70794 Filderstadt</a></h5>
			</div>
			<div class="col-md">
				<i class="fas fa-phone fa-3x" style="color:#c53500; margin-bottom: 1.5rem;"></i>
                <h5><a class="linkable" href="tel:+4915734895786">+49 157 34895786</a></h5>
			</div>
			<div class="col-md">
				<i class="fas fa-envelope fa-3x" style="color:#c53500; margin-bottom: 1.5rem;"></i>
                <h5><a class="linkable" href="mailto:info@proficlean24.de">info@proficlean24.de</a></h5>
			</div>
		</div>
	</div>
</div>

<div id="business-areas" class="offset">
<!-- Start jumbotron -->
	<div class="fixed-background">
		<div class="row dark text-center">
			<div class="col-12">

				<h3 class="heading">Unsere Geschäftsbereiche</h3>
				<div class="heading-underline"></div>
			</div>
		</div>

			<div class="row dark text-center justify-content-center">
				<div class="col-lg-6">
					<div class="business-area">
						<a class="categories-link" href="/gebäudereinigung.php">
							<button class=" btn btn-dark btn-icons">
								<img class="filter-color" id="icon-image" src="img/categories/gebauderainigung.svg">
			                    <h3>Gebäudereinigung</h3>
							</button>
						</a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="business-area">
						<a class="categories-link" href="/glasreinigung.php">
							<button class=" btn btn-dark btn-icons">
								<img class="filter-color" id="icon-image" src="img/categories/glasreinigung.svg">
								<h3>Glasreinigung</h3>
							</button>
						</a>
					</div>
				</div>
			</div>

			<div class="row dark text-center justify-content-center">
				<div class="col-lg-6">
					<div class="business-area">
						<a class="categories-link" href="/gartenpflege.php">
							<button class=" btn btn-dark btn-icons">
								<img class="filter-color" id="icon-image" src="img/categories/gartenpflege.svg">
								<h3>Gartenpflege</h3>
							</button>
						</a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="business-area">
						<a class="categories-link" href="/hausmeisterservice.php">
							<button class=" btn btn-dark btn-icons">
								<img class="filter-color" id="icon-image" src="img/categories/hausmeisterservice.svg">
								<h3>Hausmeisterservice</h3>
							</button>
						</a>
					</div>
				</div>
			</div>
		<div class="fixed-wrap">
			<div class="fixed"></div>
		</div>
	</div> <!-- End fixed bg -->
</div>



<!--- Start contact Section -->
<div id="contact">
<footer>
	<?php include 'includes/footer.php';?>
</footer>
</div>
<!--- End contact Section -->
	

<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>




