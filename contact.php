<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">

        <div class="carousel-inner" role="listbox">
            <!--- Slide 1 -->
            <div class="carousel-item contact-carousel active">
                <picture>
                    <source srcset="img/contact-us.webp" type="image/webp">
                    <source srcset="img/contact-us.jpg" type="image/jpeg">
                    <img class="d-block w-100" src="img/contact-us.jpg">
                </picture>
                <div class="carousel-caption text-center">
                    <h1 class="animate__animated animate__fadeInDown animate__delay-1s">ProfiClean24</h1>
                    <h3 class="animate__animated animate__fadeInUp animate__delay-2s">Ihre Saubere Lösung</h3>
                </div>
            </div>
        </div> <!--- End carousel inner -->
    </div>
</div>


<div id="contact-us" class="contact-us">
	<div class="row justify-content-center">
		<h3>WIE KÖNNEN WIR IHNEN HELFEN?</h3>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-3 ">
			<div class="col-md-12 mx-auto">
				<div class="card card-body card-left-form">
					<div class="row text-contact-us mt-5">
						<i class="fas fa-phone fa-2x" style="color: #e33e00; margin-right: 1.5rem;"></i>
						<a class=" contact-form-text" href="tel:+4915734895786">+49 157 34895786</a>
					</div>
					<hr class="contact-form-line">
					<div class="row text-contact-us">
						<i class="fas fa-envelope fa-2x" style="color:#e33e00; margin-right: 1.5rem;"></i>
						<a class=" contact-form-text" href="mailto:info@proficlean24.de">info@proficlean24.de</a>
					</div>
					<hr class="contact-form-line">
					<div class="row text-contact-us">
						<i class="fas fa-map-marker-alt fa-2x" style="color:#e33e00; margin-right: 1.5rem;"></i>
						<a class=" contact-form-text" href="https://maps.google.com/?q=Plieninger Str.55,70794 Filderstadt">Plieninger Str.55 <br> 70794 <br> Filderstadt</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="row contact-form">
				<div class="col-md-12 mx-auto">
					<div class="card card-body card-form">
						<h2 class="text-center mb-4">Kontaktiere Uns</h2>
						<form method='post' action="/send_email.php" class="needs-validation" novalidate>
							<div class="form-group">
								<label for="uname">Ihr Name*</label>
								<input type="text" class="form-control" id="uname" placeholder="Enter username" name="uname" required>
								<div class="invalid-feedback">Bitte füllen Sie dieses Feld aus.</div>
							</div>
							<div class="form-group">
								<label for="email">Email*</label>
								<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
								<div class="invalid-feedback">Bitte füllen Sie dieses Feld aus.</div>
							</div>
							<div class="form-group">
								<label for="phone">Telefonnummer*</label>
								<input type="number" class="form-control" id="phone" placeholder="Enter phone" name="phone" pattern="[+]{1}[0-9]{11,14}" required>
								<div class="invalid-feedback">Bitte füllen Sie dieses Feld aus.</div>
							</div>
							<div class="form-group">
								<label for="textarea">Nachricht*</label>
								<textarea class="form-control" id="textarea" name="message" rows="5"></textarea>
								<div class="invalid-feedback">Bitte füllen Sie dieses Feld aus.</div>
							</div>

							<input class="btn btn-lg btn-block btn-contact-form" value="Senden" type="submit">
						</form>
						<script>
                            // Disable form submissions if there are invalid fields
                            (function() {
                                'use strict';
                                window.addEventListener('load', function() {
                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    var forms = document.getElementsByClassName('needs-validation');
                                    // Loop over them and prevent submission
                                    var validation = Array.prototype.filter.call(forms, function(form) {
                                        form.addEventListener('submit', function(event) {
                                            if (form.checkValidity() === false) {
                                                event.preventDefault();
                                                event.stopPropagation();
                                            }
                                            form.classList.add('was-validated');
                                        }, false);
                                    });
                                }, false);
                            })();
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row justify-content-center text-center google-maps">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2634.105074939573!2d9.216192715997968!3d48.68435567927076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4799c26fa317a835%3A0xe2e2a6eb681c22e!2sPlieninger%20Str.%2055%2C%2070794%20Filderstadt%2C%20Germany!5e0!3m2!1sen!2srs!4v1608676167589!5m2!1sen!2srs" width="1440" height="720" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	</div>
</div>


<!--- Start contact Section -->
<div id="contact">
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
</div>
<!--- End contact Section -->


<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>
