<!--- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<a class="navbar-brand" href="/"><img src="img/logo.png"></a>
	<button class="navbar-toggler custom-toggler" type="button" data-trigger="#main_nav">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="navbar-collapse" id="main_nav">
		<div class="offcanvas-header mt-3">
			<a class="navbar-brand" href="/"><img src="img/logo.png"></a>
			<a class="btn btn-close float-right"><i class="fas fa-times fa-2x"></i></a>
		</div>
		<ul class="navbar-nav ml-auto pr-md-4">
			<li class="nav-item">
				<a href="/" class="nav-link">Home</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Geschäftsbereiche
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown" >
					<a class="dropdown-item " href=/gebäudereinigung.php>- Gebäudereinigung</a>
					<a class="dropdown-item" href=/glasreinigung.php>- Glasreinigung</a>
					<a class="dropdown-item" href=/gartenpflege.php>- Gartenpflege</a>
					<a class="dropdown-item" href=/hausmeisterservice.php>- Hausmeisterservice</a>
				</div>
			</li>
			<li class="nav-item">
				<a href="/unternehmen.php" class="nav-link">Unternehmen</a>
			</li>
			<li class="nav-item">
				<a href="/team.php" class="nav-link">Team</a>
			</li>
			<li class="nav-item">
				<a href="/contact.php" class="nav-link">Kontakt</a>
			</li>
			<li class="nav-item">
				<a href="/impressum.php" class="nav-link">Impressum</a>
			</li>


		</ul>
	</div>
</nav>