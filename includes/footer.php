<div class="row justify-content-center">

	<div class="col-md-5 text-center">
		<img src="img/logo.png">
		<br>
		<strong>Was können wir für Sie tun?</strong>
        <p> Jetzt anrufen: <a class="linkable" href="tel:+4915734895786"><strong>+49 157 34895786</strong></a> <br><br> Schreiben Sie uns: <a class="linkable" href="mailto:info@proficlean24.de"><strong>info@proficlean24.de</strong> </a> <br><br> <strong> ProfiClean24 </strong><br>Plieninger Str.55 <br> 70794 Filderstadt</p>
	</div>
	<hr class="socket">
	&copy ProfiClean24
</div>
<br>
<div class="row justify-content-center footer-notes">
	<h6 class="madeby"> Icons made by <a href="https://www.flaticon.com/authors/catkuro" title="catkuro">catkuro</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></h6>
</div>

<div class="row justify-content-center footer-notes">
	<h6 class="madeby">Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></h6>
</div>

<div class="row justify-content-center footer-notes">
	<h6 class="madeby">Icons made by <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></h6>
</div>



<a id="back-to-top" href="#" class="btn btn-dark btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>




