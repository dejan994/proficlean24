<?php

$errors = [];

if (!empty($_POST)) {
	$name = $_POST['uname'];
	$number = "+49".$_POST['phone'];
	$email = $_POST['email'];
	$message = $_POST['message'];

	if (empty($name)) {
		$errors[] = 'Name is empty';
	}

	if (empty($email)) {
		$errors[] = 'Email is empty';
	} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$errors[] = 'Email is invalid';
	}

	if (empty($message)) {
		$errors[] = 'Message is empty';
	}
}

if (!empty($errors)) {
	$allErrors = join('<br/>', $errors);
	$errorMessage = "<p style='color: red;'>{$allErrors}</p>";
} else {
	$toEmail = 'info@proficlean24.de';
	$emailSubject = 'Neue E-Mail von proficlean24';
	$headers = ['From' => $email, 'Reply-To' => $email, 'Content-type' => 'text/html; charset=iso-8859-1'];

	$bodyParagraphs = ["Name: {$name} <br><br>", "Email: {$email} <br><br>", "Nachricht:", $message];
	$body = join(PHP_EOL, $bodyParagraphs);

	if (mail($toEmail, $emailSubject, $body, $headers)) {
		?>
			<script>
				alert('Ihre E-Mail wurde gesendet. Vielen Dank!');
			</script>
		<meta HTTP-EQUIV = "Refresh" content="0; url=contact.php">
		<?php
	} else {
		?>
		<script>
            alert('Oops, something went wrong. Please try again later');
		</script>
		<meta HTTP-EQUIV = "Refresh" content="0; url=contact.php">
		<?php
	}
}

