<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/header.php'; ?>
</head>

<body data-spy="scroll" data-target="#navbarResponsive">

<!--- Start Home Section -->
<div id="home">
	<b class="screen-overlay"></b>

	<?php include 'includes/navigation.php'; ?>

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">

		<div class="carousel-inner" role="listbox">
			<!--- Slide 1 -->
			<div class="carousel-item contact-carousel active">
				<picture>
					<source srcset="img/glasreinigung/glasreinigung-1.webp" type="image/webp">
					<source srcset="img/glasreinigung/glasreinigung-1.jpg" type="image/jpeg">
					<img class="d-block w-100" src="img/glasreinigung/glasreinigung-1.jpg">
				</picture>
				<div class="carousel-caption-categories text-center">
					<h1 class="animate__animated animate__fadeInDown animate__delay-1s">Glasreinigung</h1>
				</div>
			</div>

		</div> <!--- End carousel inner -->
	</div>

</div>

<div id="glasreinigung" class="text-under-carousel">
	<div class="row unternehmen-row justify-content-center">
		<div class="col-md-4 justify-content-center">
			<h2 >Glasreinigung</h2>
			<hr class="heading-underline-left">
			<h3><b>Folgende Dienstleistungen bieten wir im Bereich Glasreinigung an:</b></h3>
			<ul>
				<li>Glasreinigung</li>
				<li>Reinigung von Beleuchtungskörpern</li>
				<li>Rahmenreinigung</li>
				<li>Reinigung von Glastrennwänden</li>
				<li>Reinigung von Glassfassaden</li>
			</ul>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

<!--- Start contact Section -->
<div id="contact">
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
</div>
<!--- End contact Section -->


<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.6.1/js/all.js"></script>
<script src="js/main.js"></script>
<!--- End of Script Source Files -->

</body>
</html>